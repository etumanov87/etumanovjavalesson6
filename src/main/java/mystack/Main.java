package mystack;

public class Main {
    public static void main(String[] args) {
        MyStack<String> myStack = new MyStack<>(10);
        myStack.pop();
        myStack.push("Элемент номер один");
        System.out.println(myStack);
        myStack.pop();
        System.out.println(myStack);
        myStack.pop();
    }

}
