package mystack;

import java.util.Arrays;

/**
 * Дженерик тип T нужен для использования любого типа объектов в стеке.
 * Методы push и pop обновлены, теперь они проверяют размер стека перед добавлением или извлечением элементов.
 * Добавил Конструктор
 */
public class MyStack<T> {
    private final int maxSize;
    private final T[] elements;
    private int index;

    public MyStack(int size) {
        this.maxSize = size;
        this.elements = (T[]) new Object[size];
        this.index = -1;
    }

    public void push(T item) {
        if (index < maxSize - 1) {
            elements[++index] = item;
        } else {
            System.out.println("Стек заполнен. Невозможно положить элемент.");
        }
    }

    public T pop() {
        if (index >= 0) {
            T removeElement = elements[index];
            elements[index] = null;
            index--;
            return removeElement;

        } else {
            System.out.println("Стек пуст. Невозможно извлечь элемент.");
            return null;
        }
    }

    @Override
    public String toString() {
        return "MyStack{" +
                "maxSize=" + maxSize +
                ", elements=" + Arrays.toString(elements) +
                '}';
    }
}


