package calculator;

import java.util.Arrays;

import static calculator.Operation.*;


public class Calculator {

    public static double calculator(double num1, String operation, double num2) {
        double result = 0;
        try {
            switch (operation) {
                case "+", "плюс" -> result = Math.ceil(sum(num1, num2) * 100) / 100;
                case "-", "минус" -> result = Math.ceil(subtract(num1, num2) * 100) / 100;
                case "*", "умножить" -> result = Math.ceil(multiply(num1, num2) * 100) / 100;
                case "/", "делить" -> {
                    if (num2 == 0) {
                        throw new InfiniteException("На 0 делить нельзя!");
                    }
                    result = Math.ceil(divide(num1, num2) * 100) / 100;
                }
                default -> System.out.println("Неправильный оператор");
            }
        } catch (Exception e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
            System.out.println(e.getMessage());
        }
        return result;
    }
}

