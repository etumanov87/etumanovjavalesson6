package calculator;

import java.util.Arrays;
import java.util.List;

public abstract class Operation {
    static double sum(double a, double b) {
        return a + b;
    }

    static double subtract(double a, double b) {
        return a - b;
    }

    static double multiply(double a, double b) {
        return a * b;
    }

    static double divide(double a, double b) {
        return a / b;
    }
}
