package calculator;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println("Введите первый агруент: ");
            double num1 = scanner.nextDouble();

            System.out.println("Введите оператор (+, -, *, /, плюс, минус, умножить, делить): ");
            String operation = scanner.next();

            System.out.println("Введите второй агруент: ");
            double num2 = scanner.nextDouble();
            System.out.println("Результат вычисления: " + Calculator.calculator(num1, operation, num2));
        } catch (InputMismatchException e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }
    }
}
