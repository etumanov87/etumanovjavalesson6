package calculator;

public class InfiniteException extends Exception {
    public InfiniteException(String message) {
        super(message);
    }
}